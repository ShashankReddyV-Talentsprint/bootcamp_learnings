\documentclass{beamer}

\usetheme{Madrid}
\usepackage{hyperref}

\title{WEC6 May Bootcamp Learnings}
\author{FutureTech Fellows}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}{Technical Learning}
    1. Functional decomposition: Each function should do only one job and well.
    
    2. Writing human readable code: Using constants and professional coding practices.
    
    3. Data structuring based on logic.
    
    4. Using GIT and remote machines for collaborative work.
    
    5. Cryptography: Encryption, Decryption, importance of mathematics in implementation.
    
    6. Importance of errors: Learning through unit testing.
    
    7. Understanding and utilizing estimated time effectively.
\end{frame}

\begin{frame}{Teaching Methodology}
    1. Impact of giving time after problem statement.
    
    2. Seating arrangements, random grouping, and code reading to build confidence and teamwork.
    
    3. Competitiveness: Appreciating when Speaking out who find bugs and implement better logic.
    
    4. Importance of speaking out over local discussion.
    
    5. Coding while presenting: Students suggestion and find mistakes.
    
    6. Keeping up with the flow and adapting content based on students.
    
    7. Lifelong learning: Even experienced engineers have much to learn.
\end{frame}

\begin{frame}{}
    1. Usage of tools like \textbf{LaTeX}, \textbf{Pelican}, \textbf{Sphinx}, \textbf{GraphViz}, and the advantage of \textbf{WSL} on Windows.
    
    2. Compared JavaScript and Python code on the same problem: ISBN Number Problem, Luhn Algorithm, and Odometer. Thanks to Professor Vanchinathan for demonstrating the usefulness of number theory.
    
    3. Used Classes to complete problems from Week 2 (Odometer). Found games like Wordle and Diamonds interactive. Thanks to Asokan for the game problems.
    
    4. Create meeting links and present work to all students to ensure visibility.
    
    5. Encourage trainees to read and then watch videos if needed for understanding.
    
    6. Encourage writing problems on paper before using a PC to avoid disappointment from errors.
    
    7. Write calculated mistakes while coding to check trainee understanding.
\end{frame}
\end{document}